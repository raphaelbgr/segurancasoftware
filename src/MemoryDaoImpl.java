import java.util.ArrayList;
import java.util.List;

public class MemoryDaoImpl implements DAO {

	public static List<Emp> empList = new ArrayList<Emp>();

	@Override
	public int save(Emp e) {
		e.setId(empList.size());
		empList.add(e);
		return 1;
	}

	@Override
	public int update(Emp e) {
		Emp oldEmp = empList.stream()
        .filter(x -> e.getId() == x.getId())
        .findAny()
        .orElse(null);
		if (oldEmp != null) {
			empList.remove(oldEmp);
			empList.add(e);
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int delete(int id) {
		Emp emp = empList.stream()
                .filter(x -> id == x.getId())
                .findAny()
                .orElse(null);
		if (emp != null) {
			empList.remove(emp);
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public Emp getEmployeeById(int id) {
		return empList.stream()
                .filter(x -> id == x.getId())
                .findAny()
                .orElse(new Emp());
	}

	@Override
	public List<Emp> getAllEmployees() {
		return empList;
	}

}
