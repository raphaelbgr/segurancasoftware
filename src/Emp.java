public class Emp {
	
	private int id;  
	private String name,password,email,country;  
	
	public int getId() {  
		return id;  
	}  
	public void setId(int id) {  
		this.id = id;  
	}  
	public String getName() {  
		return name != null ? name : "No name";  
	}  
	public void setName(String name) {  
		this.name = name;  
	}  
	public String getPassword() {  
		return password != null ? password : "1234";  
	}  
	public void setPassword(String password) {  
		this.password = password;  
	}  
	public String getEmail() {  
		return email != null ? email : "No email";  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	}  
	public String getCountry() {  
		return country != null ? country : "No country";  
	}  
	public void setCountry(String country) {  
		this.country = country;  
	}  

}