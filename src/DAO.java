import java.util.List;  
  
public interface DAO {  
  
    public int save(Emp e);
    public int update(Emp e);
    public int delete(int id);
    public Emp getEmployeeById(int id);
    public List<Emp> getAllEmployees();
}